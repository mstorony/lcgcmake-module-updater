#! /usr/bin/env python

import os
import re
import git
import shutil
import fileinput
import tarfile
import sys
from datetime import datetime
from git import TagReference, Repo
from jenkinsapi.jenkins import Jenkins

if len(sys.argv) <= 1:
    print("Please specify the heptools suffix as argument to this script")
    exit(-1)

dependenciesFilePath = "heptools-" + sys.argv[1] + ".cmake"
cmakeExternals = "/afs/cern.ch/user/m/mstorony/Documents/lcgcmake/externals/CMakeLists.txt"
cmakePyExternals = "/afs/cern.ch/user/m/mstorony/Documents/lcgcmake/pyexternals/CMakeLists.txt"
jenkinsUsername = "someone"
jenkinsPassword = "secret"
jenkinsBase = "http://localhost:8080"
jenkinsLogLocation = "/var/log/jenkins/jenkins.log"
jobToExecute = "Test3"

modulePattern = re.compile("^LCG_external_package\((\S*)\s*(\S*)")

#Gets the module version for a specific name in the globally specified cmake file (dependenciesFilePath)
def getModuleVersion(moduleName):
    with open(dependenciesFilePath) as dependenciesFile:
        for line in dependenciesFile:
            module = modulePattern.match(line) 
            if module and module.group(1) == moduleName:
                return module.group(2)
                
    return None

#Creates a tar file of a folder with a specific file name
def makeTarFile(folder, fileName):
    print "Taring {0}".format(folder)
    archive = tarfile.open(fileName, "w|gz")
    archive.add(folder, arcname="/")
    archive.close()
    print "Tarred folder {0} into {1}".format(folder, fileName)

#Triggers the Jenkins job
def triggerJob():
    #J = Jenkins(jenkinsBase, jenkinsUsername, jenkinsPassword)
    J = Jenkins(jenkinsBase)
    J.build_job(jobToExecute)
    print ("Triggered job")

#Called once a change is made to a repo
def onGitHubTrigger(repoUrl):
    print "New repo has been triggered: {0}".format(repoUrl);
    
    repoNameIndex = repoUrl.rfind("/")
    repoName = repoUrl[repoNameIndex + 1:]

    if not os.path.isdir("repos"):
        os.mkdir("repos")

    repoLocation = "repos/" + repoName

    if os.path.isdir(repoLocation):
        shutil.rmtree(repoLocation)

    os.mkdir(repoLocation)

    print "Cloning into {0}".format(repoUrl)
    repo = Repo.clone_from(repoUrl, repoLocation)
    print "Cloned repo"

    lastCommit = repo.head.commit

    print "Last commit was: {0}".format(lastCommit.hexsha)

    tagVersion = None
    for tagRef in repo.tags:
        if tagRef.commit.hexsha == lastCommit.hexsha:
            print "New version came with last commit: {0}".format(lastCommit.hexsha)
            tagVersion = str(tagRef)

    if not existingModule(repoName, cmakeExternals) and not existingModule(repoName, cmakePyExternals):
        print "{0} is not a recognized module by cmake".format(repoName)
    elif tagVersion is not None:
        print "New version has been submitted!"
        print "taring directory"

        if tagVersion.startswith("v"):
            tagVersion = tagVersion[1:]

        tarFileName = "{0}-{1}.tar.gz".format(repoName, tagVersion)

        makeTarFile(repoLocation, tarFileName)
        oldVersion = getModuleVersion(repoName)
        upgradeVersion(repoName, oldVersion, tagVersion)
        triggerJob()

def existingModule(moduleName, cmakeList):
    with open(cmakeList) as cmakeListFile:
        for line in cmakeListFile:
            if moduleName in line:
                return True
    return False

#Parses the Jenkins logfile and looks for events related to changes in GitHub repos
def parseLogFile(logFilePath, persistentDateFilePath):
    logEntryHeaderPattern = re.compile("^([A-Z][a-z]{2} [0-9]+, [0-9]{4} [0-9]+:[0-9]+:[0-9]+ (AM|PM)) ([a-zA-Z._]*) ([a-zA-Z_]*)$")
    hookEventPattern = re.compile("^INFO: Received POST for (https?:\/\/([^\/]+)\/([^\/]+)\/([^\/]+)\/?)$")

    lastLogEntry = None
    parseEntries = False

    if os.path.isfile(persistentDateFilePath):
        with open(persistentDateFilePath) as dateFile:
            line = dateFile.readline()
            lastLogEntry = datetime.strptime(line, "%Y-%m-%d %H:%M:%S")

    with open(logFilePath) as jenkinsLogFile:
        for line in jenkinsLogFile:
            logEntryHeader = logEntryHeaderPattern.match(line)
            hookEvent = hookEventPattern.match(line)

            if logEntryHeader:
                logTime = datetime.strptime(logEntryHeader.group(1), "%b %d, %Y %X %p")
                
                if lastLogEntry == None or logTime > lastLogEntry:
                    parseEntries = True
                    lastLogEntry = logTime

            elif parseEntries == True and hookEvent:
                onGitHubTrigger(hookEvent.group(1).strip())
            

#Upgrades the version number of a specific module
def upgradeVersion(moduleName, oldVersion, newVersion):
    dependenciesFile = fileinput.FileInput(dependenciesFilePath, inplace=True)
    for line in dependenciesFile:
        module = modulePattern.match(line) 
        if module and module.group(1) == moduleName and module.group(2) == oldVersion:
            print line.replace(oldVersion, newVersion)
        else:
            print line

    dependenciesFile.close()


parseLogFile(jenkinsLogLocation, "lastLogEntryDate")