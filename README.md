# About
This script is used to automatically update modules in lcgcmake based on received web-hook requests from GitHib. The script scans the Jenkins log-file in order to determine whether there has been any change to a GitHub repository that contains a module used in lcgcmake. Once there is a new version on a repository, the script will clone the repository and create a tar-file of the clones repository, put it on AFS with the new version number, update the version number in the heptools file specified and then start the job for lcgcmake in Jenkins. 

# How to set up

* Edit `check-gh-triggers.gh` and update the following variables:


| Variable name      |  Description                                                                |  Example
| ------------------ | --------------------------------------------------------------------------- | -------------------------------- |
| lcgcmake           |  Location to lcgcmake                                                       |  `/home/you/Documents/lcgcmake`  |
| jenkinsUsername    |  Username of the user to trigger the Jenkins job                            |  `someone`                       |
| jenkinsPassword    |  Password of the user to trigger the Jenkins job                            |  `somethingsecret`               |
| jenkinsBase        |  The URL to the Jenkins instance that should execute the job                |  `https://phsft-jenkins.cern.ch` |
| jenkinsLogLocation |  Location of the Jenkins logfile                                            |  `/var/log/jenkins/jenkins.log`  |
| jobToExecute       |  Name of the job to trigger in Jenkins once there is a new version updated  |  `lcg_ext_devn`                  |


* Install the required dependencies e.g. by using pip:

`# python -m pip install jenkinsapi`

`# python -m pip install gitpython`

If you don't have pip installed, see [this](https://pip.pypa.io/en/stable/installing/) guide.


* Set up a job in Jenkins that triggers the python script on a desired interval.

* Set up a web-hook between GitHub and Jenkins for the repositories that should be automatically updated.

# Usage

`$ python check-gh-triggers.py <heptools suffix>`

Where `<heptools suffix>` is the suffix for the heptools-file inside `lcgcmake/cmake/toolchain`. If e.g. `experimental` is specified, `heptools-experimental.cmake` will have its version numbers updated on version updates.

# Dependencies
- jenkinsapi
- gitpythonCommit